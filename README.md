OpenWRT managable by Ansible
============================

Setting OpenWRT to be managable by Ansible.

This role will install:
- python-light
- python-codecs
- python-logging
- python-openssl
- python-distutils
- openssh-sftp-server


Requirements
------------

- OpenWRT as target system
- Root access
- Read and apply Importants Notes section


Importants notes
--------------

### Note 1: Ansible config ###

By default, OpenWRT doesn't have SFTP on it's SSH server. So, you need
to use `scp` when you apply this role for the first time.

For this, use this command:

    ANSIBLE_CONFIG=roles/openwrt-manage/ansible.cfg ansible-playbook -i hosts my_playbook.yml

Where `my_playbook.yml` apply this role to your OperWRT systems.
This role will install a SFTP server, so next time you don't need this special config.


### Note 2: Ansible gathering facts ###

Because python is not installed by default in OpenWRT systems, you
need to disable gathering facts on the playbook that apply this role.
For this, add `gather_facts: no` to this playbook.

After this role is applied, python is installed. So, no need to
disable gathering fact anymore.


Example Playbook
----------------

    - hosts: servers
	  user: root
      gather_facts: no
      roles:
         - openwrt-manage


License
-------

GPLv3


Author Information
------------------

Sébastien Gendre <s.gendre@openmailbox.org>
